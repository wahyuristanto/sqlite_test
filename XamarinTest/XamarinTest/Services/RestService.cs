﻿using System;
using System.Collections.Generic;
using System.Net.Http;
using System.Text;
using System.Threading.Tasks;

namespace XamarinTest.Services
{
    public class RestService : IRestService
    {
        public async Task<T> GetAsync<T>(string api)
        {
            var client = new HttpClient();
            T model = default(T);
            try
            {
                //client.Timeout = TimeSpan.FromSeconds(10);
                //client.MaxResponseContentBufferSize = int.MaxValue;
                //client.BaseAddress = new Uri(Constants.BASE_URL);
                HttpResponseMessage response = await client.GetAsync($"{Constants.BASE_URL}/v2/{api}");
                if (response.IsSuccessStatusCode)
                {
                    var data = await response.Content.ReadAsStringAsync();
                    model = Newtonsoft.Json.JsonConvert.DeserializeObject<T>(data);
                }
            }
            catch (Exception ex)
            {
                string msg = ex.Message;
                System.Diagnostics.Debug.WriteLine($"Error : {msg} | {ex.StackTrace}");
                return model;
            }
            return model;
        }

    }
    public interface IRestService
    {
        Task<T> GetAsync<T>(string api);

    }
}
