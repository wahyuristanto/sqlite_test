﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using SQLite;
using XamarinTest.Models;

namespace XamarinTest.Data
{
    public class UsersDatabase
    {
        static readonly Lazy<SQLiteAsyncConnection> lazyInitializer = new Lazy<SQLiteAsyncConnection>(() =>
        {
            return new SQLiteAsyncConnection(Constants.DatabasePath, Constants.Flags);
        });

        static SQLiteAsyncConnection Database => lazyInitializer.Value;
        static bool initialized = false;

        public UsersDatabase()
        {
            InitializeAsync().SafeFireAndForget(false);
        }

        async Task InitializeAsync()
        {
            if (!initialized)
            {
                if (!Database.TableMappings.Any(m => m.MappedType.Name == typeof(Users).Name))
                {
                    await Database.CreateTablesAsync(CreateFlags.None, typeof(Users)).ConfigureAwait(false);
                    initialized = true;
                }
            }
        }

        public Task<List<Users>> GetItemsAsync()
        {
            return Database.Table<Users>().ToListAsync();
        }

        public Task<List<Users>> GetItemsNotDoneAsync()
        {
            return Database.QueryAsync<Users>("SELECT * FROM [Users] WHERE [IsEnable] = 0");
        }

        public Task<Users> GetItemAsync(int id)
        {
            return Database.Table<Users>().Where(i => i.ID == id).FirstOrDefaultAsync();
        }

        public Task<int> SaveItemAsync(Users item)
        {
            if (item.ID != 0)
            {
                return Database.UpdateAsync(item);
            }
            else
            {
                return Database.InsertAsync(item);
            }
        }

        public Task<int> DeleteItemAsync(Users item)
        {
            return Database.DeleteAsync(item);
        }

        public Task<Users> GetLoginAsync(string UserId, string pwd)
        {
            return Database.Table<Users>().Where(i => i.UserId == UserId && i.Password == pwd).FirstOrDefaultAsync();
        }
    }

}
