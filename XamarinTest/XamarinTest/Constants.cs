﻿using System;
using System.Collections.Generic;
using System.IO;
using System.Text;

namespace XamarinTest
{
    public static class Constants
    {
        public const string DatabaseFilename = "TodoSQLite.db3";
        public const string BASE_URL = "https://corona.lmao.ninja";

        public const SQLite.SQLiteOpenFlags Flags =
            SQLite.SQLiteOpenFlags.ReadWrite |
            SQLite.SQLiteOpenFlags.Create |
            SQLite.SQLiteOpenFlags.SharedCache;

        public static string DatabasePath
        {
            get
            {
                var basePath = Environment.GetFolderPath(Environment.SpecialFolder.LocalApplicationData);
                return Path.Combine(basePath, DatabaseFilename);
            }
        }
    }
}
