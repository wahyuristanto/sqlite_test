﻿using SQLite;
using System;
using System.Collections.Generic;
using System.Text;

namespace XamarinTest.Models
{
    public class Users
    {
        [PrimaryKey, AutoIncrement]
        public int ID { get; set; }
        public string UserId { get; set; }
        public string Password { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public bool IsEnable { get; set; }
    }
}
