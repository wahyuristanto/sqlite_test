﻿using System;
using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTest.Data;
using XamarinTest.Services;
using XamarinTest.Views;

namespace XamarinTest
{
    public partial class App : Application
    {
        static UsersDatabase database;

        public App()
        {
            InitializeComponent();

            DependencyService.Register<MockDataStore>();


            if (Xamarin.Essentials.Preferences.ContainsKey("IsLogin"))
            {
                MainPage = new MainPage();
            }else
                MainPage = new LoginPage();
        }
        public static UsersDatabase Database
        {
            get
            {
                if (database == null)
                {
                    database = new UsersDatabase();
                }
                return database;
            }
        }


        protected override void OnStart()
        {
        }

        protected override void OnSleep()
        {
        }

        protected override void OnResume()
        {
        }
    }
}
