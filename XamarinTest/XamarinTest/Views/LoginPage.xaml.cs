﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTest.Models;

namespace XamarinTest.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class LoginPage : ContentPage
    {
        public LoginPage()
        {
            InitializeComponent();
        }
        async private void OnLogin_Clicked(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(UserId.Text) || string.IsNullOrEmpty(Password.Text))
            {
                await DisplayAlert("Warning", "UserId dan password tidak lengkap", "OK");
                return;
            }
            var user = await App.Database.GetLoginAsync(UserId.Text, Password.Text);

            if (user == null)
            {
                await DisplayAlert("Warning", "UserId dan password salah", "OK");
                return;
            }

            Xamarin.Essentials.Preferences.Set("IsLogin", true);
            Xamarin.Essentials.Preferences.Set("UserId", user.ID);
            Xamarin.Essentials.Preferences.Set("UserName", user.UserId);

            App.Current.MainPage = new MainPage();
        }

        async private void OnRegister_Clicked(object sender, EventArgs e)
        {
            await Navigation.PushModalAsync(new RegisterPage
            {
                BindingContext = new Users()
            });
        }

    }
}