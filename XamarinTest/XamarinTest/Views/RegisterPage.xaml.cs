﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTest.Models;

namespace XamarinTest.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class RegisterPage : ContentPage
    {
        public RegisterPage()
        {
            InitializeComponent();
        }
        async private void OnSave_Clicked(object sender, EventArgs e)
        {
            var user = (Users)BindingContext;
            user.IsEnable = true;
            await App.Database.SaveItemAsync(user);
            await Navigation.PopModalAsync();
        }
    }
}