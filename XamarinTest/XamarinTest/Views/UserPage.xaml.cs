﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

using Xamarin.Forms;
using Xamarin.Forms.Xaml;
using XamarinTest.Models;

namespace XamarinTest.Views
{
    [XamlCompilation(XamlCompilationOptions.Compile)]
    public partial class UserPage : ContentPage
    {
        public UserPage()
        {
            InitializeComponent();
        }
        protected override async void OnAppearing()
        {
            base.OnAppearing();
            UserId.Text = Xamarin.Essentials.Preferences.Get("UserName", "UserName");


            listView.ItemsSource = await App.Database.GetItemsAsync();
        }

        async void OnItemAdded(object sender, EventArgs e)
        {
            await Navigation.PushAsync(new UserDetailPage
            {
                BindingContext = new Users()
            });
        }

        async void OnListItemSelected(object sender, SelectedItemChangedEventArgs e)
        {
            if (e.SelectedItem != null)
            {
                await Navigation.PushAsync(new UserDetailPage
                {
                    BindingContext = e.SelectedItem as Users
                });
            }
        }

        private void OnLogout_Clicked(object sender, EventArgs e)
        {
            Xamarin.Essentials.Preferences.Set("UserId", 0);
            Xamarin.Essentials.Preferences.Set("UserName", "");

            var nav = new NavigationPage(new Views.LoginPage());
            App.Current.MainPage = nav;
        }
    }
}