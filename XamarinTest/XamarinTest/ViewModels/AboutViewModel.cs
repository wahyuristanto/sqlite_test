﻿using System;
using System.Threading.Tasks;
using System.Windows.Input;
using Xamarin.Essentials;
using Xamarin.Forms;
using XamarinTest.Helpers;
using XamarinTest.Models;

namespace XamarinTest.ViewModels
{
    public class AboutViewModel : BaseViewModel
    {
        Services.IRestService service;
        public AboutViewModel()
        {
            service = new Services.RestService();

            Title = "About";
            OpenWebCommand = new Command(async () => await Browser.OpenAsync("https://xamarin.com"));
        }

        public ICommand OpenWebCommand { get; }


        public async Task GetAll()
        {
            try
            {
                var r = await this.service.GetAsync<Models.GlobalTotals>("all");

                if (r != null)
                {
                    r.UpdateAt = r.updated.TransformLongToDateTime().FormatDateTime("");
                    GlobalTotals = r;

                }
            }
            catch (Exception)
            {

            }
        }

        GlobalTotals globalTotals;
        public GlobalTotals GlobalTotals
        {
            get => globalTotals;
            set => SetProperty(ref globalTotals, value, "GlobalTotals");
        }
    }
}