﻿namespace WindowsFormsApp
{
    partial class PenggajianForm
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.chkTetap = new System.Windows.Forms.CheckBox();
            this.chkKontrak = new System.Windows.Forms.CheckBox();
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.chkProbation = new System.Windows.Forms.CheckBox();
            this.txtGapok = new System.Windows.Forms.TextBox();
            this.label1 = new System.Windows.Forms.Label();
            this.btnProcess = new System.Windows.Forms.Button();
            this.lblThp = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            this.SuspendLayout();
            // 
            // chkTetap
            // 
            this.chkTetap.AutoSize = true;
            this.chkTetap.Location = new System.Drawing.Point(6, 31);
            this.chkTetap.Name = "chkTetap";
            this.chkTetap.Size = new System.Drawing.Size(77, 17);
            this.chkTetap.TabIndex = 0;
            this.chkTetap.Tag = "1";
            this.chkTetap.Text = "Permanent";
            this.chkTetap.UseVisualStyleBackColor = true;
            this.chkTetap.CheckedChanged += new System.EventHandler(this.TypeEmployee_CheckedChanged);
            // 
            // chkKontrak
            // 
            this.chkKontrak.AutoSize = true;
            this.chkKontrak.Location = new System.Drawing.Point(6, 54);
            this.chkKontrak.Name = "chkKontrak";
            this.chkKontrak.Size = new System.Drawing.Size(66, 17);
            this.chkKontrak.TabIndex = 1;
            this.chkKontrak.Tag = "2";
            this.chkKontrak.Text = "Contract";
            this.chkKontrak.UseVisualStyleBackColor = true;
            this.chkKontrak.CheckedChanged += new System.EventHandler(this.TypeEmployee_CheckedChanged);
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.chkProbation);
            this.groupBox1.Controls.Add(this.chkTetap);
            this.groupBox1.Controls.Add(this.chkKontrak);
            this.groupBox1.Location = new System.Drawing.Point(12, 12);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(131, 110);
            this.groupBox1.TabIndex = 2;
            this.groupBox1.TabStop = false;
            this.groupBox1.Text = "Type Employee";
            // 
            // chkProbation
            // 
            this.chkProbation.AutoSize = true;
            this.chkProbation.Location = new System.Drawing.Point(6, 77);
            this.chkProbation.Name = "chkProbation";
            this.chkProbation.Size = new System.Drawing.Size(71, 17);
            this.chkProbation.TabIndex = 2;
            this.chkProbation.Tag = "2";
            this.chkProbation.Text = "Probation";
            this.chkProbation.UseVisualStyleBackColor = true;
            this.chkProbation.CheckedChanged += new System.EventHandler(this.TypeEmployee_CheckedChanged);
            // 
            // txtGapok
            // 
            this.txtGapok.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.txtGapok.Location = new System.Drawing.Point(149, 43);
            this.txtGapok.Name = "txtGapok";
            this.txtGapok.Size = new System.Drawing.Size(107, 26);
            this.txtGapok.TabIndex = 3;
            this.txtGapok.TextChanged += new System.EventHandler(this.txtGapok_TextChanged);
            // 
            // label1
            // 
            this.label1.AutoSize = true;
            this.label1.Location = new System.Drawing.Point(149, 27);
            this.label1.Name = "label1";
            this.label1.Size = new System.Drawing.Size(65, 13);
            this.label1.TabIndex = 4;
            this.label1.Text = "Basic Salary";
            // 
            // btnProcess
            // 
            this.btnProcess.Location = new System.Drawing.Point(149, 72);
            this.btnProcess.Name = "btnProcess";
            this.btnProcess.Size = new System.Drawing.Size(107, 48);
            this.btnProcess.TabIndex = 5;
            this.btnProcess.Text = "Process";
            this.btnProcess.UseVisualStyleBackColor = true;
            this.btnProcess.Click += new System.EventHandler(this.btnProcess_Click);
            // 
            // lblThp
            // 
            this.lblThp.Font = new System.Drawing.Font("Microsoft Sans Serif", 18F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
            this.lblThp.Location = new System.Drawing.Point(262, 72);
            this.lblThp.Name = "lblThp";
            this.lblThp.Size = new System.Drawing.Size(156, 48);
            this.lblThp.TabIndex = 6;
            this.lblThp.Text = "0";
            this.lblThp.TextAlign = System.Drawing.ContentAlignment.MiddleRight;
            // 
            // PenggajianForm
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(430, 141);
            this.Controls.Add(this.lblThp);
            this.Controls.Add(this.btnProcess);
            this.Controls.Add(this.label1);
            this.Controls.Add(this.txtGapok);
            this.Controls.Add(this.groupBox1);
            this.Name = "PenggajianForm";
            this.Text = "PenggajianForm";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            this.ResumeLayout(false);
            this.PerformLayout();

        }

        #endregion

        private System.Windows.Forms.CheckBox chkTetap;
        private System.Windows.Forms.CheckBox chkKontrak;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.CheckBox chkProbation;
        private System.Windows.Forms.TextBox txtGapok;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Button btnProcess;
        private System.Windows.Forms.Label lblThp;
    }
}