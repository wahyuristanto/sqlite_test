﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp
{
    public partial class BilanganPrimaForm : Form
    {
        public BilanganPrimaForm()
        {
            InitializeComponent();
        }

        private void txtValue_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtValue.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                txtValue.Text = txtValue.Text.Remove(txtValue.Text.Length - 1);
            }
        }

        private void btnProcess_Click(object sender, EventArgs e)
        {
            textBox1.Text = "";
            bool prima = true;
            int bilangan = int.Parse(txtValue.Text);

            if (bilangan >= 2)
            {
                //loop hingga batas bilangan tercapai
                for (int i = 2; i <= bilangan; i++)
                {
                    //loop mulai dari 2 ke i
                    for (int j = 2; j < i; j++)
                    {
                        //bukan blangan prima jika i habis dibagi j
                        if ((i % j) == 0)
                        {
                            prima = false;
                            break;
                        }
                    }
                    //jika bilangan == prima
                    if (prima)
                        textBox1 .Text += i + "\r\n";
                    prima = true;
                }
            }
            else
                MessageBox.Show("tidak ada bilangan prima");
        }
    }
}
