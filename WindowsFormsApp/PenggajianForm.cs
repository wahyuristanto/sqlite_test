﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;

namespace WindowsFormsApp
{
    public partial class PenggajianForm : Form
    {
        public PenggajianForm()
        {
            InitializeComponent();
        }
        double potonganTambahan = 0;
        double TaxEmployee = 2;
        private void btnProcess_Click(object sender, EventArgs e)
        {
            if (string.IsNullOrEmpty(txtGapok.Text))
            {
                MessageBox.Show("Please Input Basic salary Employee");
            }
            if (chkKontrak.Checked)
            {
                potonganTambahan = Convert.ToDouble(chkKontrak.Tag);
            }
            else if (chkProbation.Checked)
            {
                potonganTambahan = Convert.ToDouble(chkProbation.Tag);
            }
            else if (chkTetap.Checked)
            {
                potonganTambahan = Convert.ToDouble(chkTetap.Tag);
            }
            else
            {
                MessageBox.Show("Please select Type Employee");
                return;
            }


            double harga = Convert.ToDouble(txtGapok.Text);
            double potongan;
            double total;
            potongan = TaxEmployee + potonganTambahan;
            total = harga - (harga * potongan / 100);
            lblThp.Text = string.Format("Rp. {0:#,#.}", total);
        }


        private void TypeEmployee_CheckedChanged(object sender, EventArgs e)
        {
            if (((CheckBox)sender).Checked)
            {


                switch (((CheckBox)sender).Text)
                {
                    case "Probation":
                        chkTetap.Checked = false;
                        chkKontrak.Checked = false;
                        break;
                    case "Contract":
                        chkTetap.Checked = false;
                        chkProbation.Checked = false;
                        break;
                    case "Permanent":
                        chkProbation.Checked = false;
                        chkKontrak.Checked = false;
                        break;
                }
            }
        }

        private void txtGapok_TextChanged(object sender, EventArgs e)
        {
            if (System.Text.RegularExpressions.Regex.IsMatch(txtGapok.Text, "[^0-9]"))
            {
                MessageBox.Show("Please enter only numbers.");
                txtGapok.Text = txtGapok.Text.Remove(txtGapok.Text.Length - 1);
            }
        }
    }
}
